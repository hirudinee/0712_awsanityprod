const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event) => {
    try {
        let data = await ddb.get({
            TableName: "1231ProdHiru",
            Key: {
                id: "2"
            }
        }).promise();
        return { "message": "Successfully executed" + data };

    } catch (err) {
        return { "message": "Error" + err }
    };

    
};